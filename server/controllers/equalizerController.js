const Equalizer = require('../models/Equalizer');

/**
 * Finds a equalizer with a given id
 * 
 * @param {any} request
 * @param {any} response
 */
function find(request, response) {
    Equalizer.findById(request.params.id).then(equalizer => response.json(equalizer))
        .catch(error => response.send(error));
}

/**
 * Gets all equalizers
 * 
 * @export
 * @param {any} request
 * @param {any} response
 */
function get(request, response) {
    Equalizer.find({}).then(equalizers => response.json(equalizers))
        .catch(error => response.send(error));
}

/**
 * Creates a new equalizer with request params
 * 
 * @param {any} request
 * @param {any} response
 */
function create(request, response) {
    Equalizer.create({
        name: request.body.name,
        low_band: request.body.low_band,
        low_peak: request.body.low_peak,
        low_freq: request.body.low_freq,
        low_gain: request.body.low_gain,
        mid_band: request.body.mid_band,
        mid_q: request.body.mid_q,
        mid_freq: request.body.mid_freq,
        mid_gain: request.body.mid_gain,
        high_mid_band: request.body.high_mid_band,
        high_mid_freq: request.body.high_mid_freq,
        high_mid_gain: request.body.high_mid_gain,
        high_band: request.body.high_band,
        high_peak: request.body.high_peak,
        high_freq: request.body.high_freq,
        high_gain: request.body.high_gain,
    }, (error, created) => {
        if (error) return response.send(error);
        return response.json(created);
    });
}

/**
 * Updates a equalizer with a given id
 * 
 * @param {any} request
 * @param {any} response
 */
function update(request, response) {
    Equalizer.findByIdAndUpdate(request.params.id, { 
        $set: {
            name: request.body.name,
            low_band: request.body.low_band,
            low_peak: request.body.low_peak,
            low_freq: request.body.low_freq,
            low_gain: request.body.low_gain,
            mid_band: request.body.mid_band,
            mid_q: request.body.mid_q,
            mid_freq: request.body.mid_freq,
            mid_gain: request.body.mid_gain,
            high_mid_band: request.body.high_mid_band,
            high_mid_freq: request.body.high_mid_freq,
            high_mid_gain: request.body.high_mid_gain,
            high_band: request.body.high_band,
            high_peak: request.body.high_peak,
            high_freq: request.body.high_freq,
            high_gain: request.body.high_gain,
        }
    }, { new: true }, (error, updated) => {
        if (error) return response.send(error);
        return response.json(updated);
    });
}


/**
 * Deletes a equalizer with a given id
 * 
 * @param {any} request
 * @param {any} response
 */
function remove(request, response) {
    Equalizer.findById(request.params.id).then((equalizer) => {
        equalizer.remove().then(deleted => response.json(deleted));
    }).catch(error => response.send(error));
}

module.exports.find = find;
module.exports.get = get;
module.exports.create = create;
module.exports.update = update;
module.exports.remove = remove;
