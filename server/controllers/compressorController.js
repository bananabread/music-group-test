const Compressor = require('../models/Compressor');

/**
 * Finds a compressor with a given id
 * 
 * @param {any} request
 * @param {any} response
 */
function find(request, response) {
    Compressor.findById(request.params.id).then(compressor => response.json(compressor))
        .catch(error => response.send(error));
}

/**
 * Gets all compressors
 * 
 * @param {any} request
 * @param {any} response
 */
function get(request, response) {
    Compressor.find({}).then(compressors => response.json(compressors))
        .catch(error => response.send(error));
}

/**
 * Creates a new compressor with request params
 * 
 * @param {any} request
 * @param {any} response
 */
function create(request, response) {
    Compressor.create({
        name: request.body.name,
        mode: request.body.mode,
        attack: request.body.attack,
        release: request.body.release,
        threshold: request.body.threshold,
        ratio: request.body.ratio,
        presence: request.body.presence,
        make_up: request.body.make_up,
    }, (error, created) => {
        if (error) return response.send(error);
        return response.json(created);
    });
}

/**
 * Updates a compressor with a given id
 * 
 * @param {any} request
 * @param {any} response
 */
function update(request, response) {
    Compressor.findByIdAndUpdate(request.params.id, { 
        $set: {
            name: request.body.name,
            mode: request.body.mode,
            attack: request.body.attack,
            release: request.body.release,
            threshold: request.body.threshold,
            ratio: request.body.ratio,
            presence: request.body.presence,
            make_up: request.body.make_up,
        }
    }, { new: true }, (error, updated) => {
        if (error) return response.send(error);
        return response.json(updated);
    });
}


/**
 * Deletes a compressor with a given id
 * 
 * @param {any} request
 * @param {any} response
 */
function remove(request, response) {
    Compressor.findById(request.params.id).then((compressor) => {
        compressor.remove().then(deleted => response.json(deleted));
    }).catch(error => response.send(error));
}

module.exports.find = find;
module.exports.get = get;
module.exports.create = create;
module.exports.update = update;
module.exports.remove = remove;
