const express = require('express');
const equalizerController = require('../controllers/equalizerController');

const router = express.Router();

router.route('/')
    .get(equalizerController.get)
    .post(equalizerController.create);

router.route('/:id')
    .get(equalizerController.find)
    .put(equalizerController.update)
    .delete(equalizerController.remove);

module.exports = router;
