const express = require('express');
const compressorRoutes = require('./compressorRoutes');
const equalizerRoutes = require('./equalizerRoutes');

const router = express.Router();

router.use('/compressor', compressorRoutes);
router.use('/equalizer', equalizerRoutes);

module.exports = router;
