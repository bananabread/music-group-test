const express = require('express');
const compressorController = require('../controllers/compressorController');

const router = express.Router();

router.route('/')
    .get(compressorController.get)
    .post(compressorController.create);

router.route('/:id')
    .get(compressorController.find)
    .put(compressorController.update)
    .delete(compressorController.remove);

module.exports = router;
