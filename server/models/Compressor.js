const mongoose = require('mongoose');

const CompressorSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    mode: {
        type: String,
        required: true,
    },
    attack: {
        type: Number,
        required: true,
    },
    release: {
        type: Number,
        required: true,
    },
    threshold: {
        type: Number,
        required: true,
    },
    ratio: {
        type: Number,
        required: true,
    },
    presence: {
        type: Number,
        required: true,
    },
    make_up: {
        type: Number,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model('Compressor', CompressorSchema);
