const mongoose = require('mongoose');

const EqualizerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    low_band: {
        type: Boolean,
        required: true,
        default: false,
    },
    low_peak: {
        type: String,
        required: true,
    },
    low_freq: {
        type: Number,
        required: true,
    },
    low_gain: {
        type: Number,
        required: true,
    },
    mid_band: {
        type: Boolean,
        required: true,
        default: false,
    },
    mid_q: {
        type: String,
        required: true,
    },
    mid_freq: {
        type: Number,
        required: true,
    },
    mid_gain: {
        type: Number,
        required: true,
    },
    high_mid_band: {
        type: Boolean,
        required: true,
        default: false,
    },
    high_mid_freq: {
        type: Number,
        required: true,
    },
    high_mid_gain: {
        type: Number,
        required: true,
    },
    high_band: {
        type: Boolean,
        required: true,
        default: false,
    },
    high_peak: {
        type: String,
        required: true,
    },
    high_freq: {
        type: Number,
        required: true,
    },
    high_gain: {
        type: Number,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Sets updated at on each save
 */
EqualizerSchema.pre('save', (next) => {
    this.updatedAt = Date.now;
    next();
});

module.exports = mongoose.model('Equalizer', EqualizerSchema);
