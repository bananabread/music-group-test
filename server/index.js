const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const bodyParser = require('body-parser');
const hbs = require('express-handlebars');
const env = require('node-env-file');
const routes = require('./routes/index');

const app = express();

env('.env');

app.use(bodyParser.json());
app.set('views', 'server/views');
app.engine('.hbs', hbs({
    extname: '.hbs',
    layoutsDir: './server/views',
}));
app.set('view engine', '.hbs');

mongoose.connect(process.env.MONGO_URL || `mongodb://mongo:27017`, {
    useMongoClient: true,
});

mongoose.connection.on('error', () => {
    console.error(`Failed to connect to mongodb: ${process.env.MONGO_URL}`);
});

app.use('/api', routes);

app.use(express.static(path.resolve('public')));

app.get('/', (request, response) => {
    response.render('index.hbs', {
        API_URL: process.env.API_URL,
    });
});

app.listen(process.env.PORT, () => {
    console.log(`Music presets app started on port: ${process.env.PORT}`);
});

module.exports = app;
