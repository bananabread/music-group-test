FROM node:8.1

RUN mkdir -p /app
WORKDIR /app
COPY . /app/

EXPOSE 8000

RUN node -v

CMD [ "yarn", "start" ]