import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import compressors from './compressorReducers';
import equalizers from './equalizerReducers';

export default combineReducers({
    compressors, equalizers, routing: routerReducer,
});
