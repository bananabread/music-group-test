export default (state = [], action) => {
    switch (action.type) {
    case 'GET_COMPRESSORS_SUCCESS': {
        return action.compressors;
    }
    case 'CREATE_COMPRESSOR_SUCCESS': {
        return [
            ...state,
            Object.assign({}, action.payload),
        ];
    }
    case 'UPDATE_COMPRESSOR_SUCCESS': {
        return state.map((compressor) => {
            if (compressor._id !== action.payload._id) return compressor;
            return action.payload;
        });
    }
    case 'DELETE_COMPRESSOR_SUCCESS': {
        return state.filter(compressor => (compressor._id !== action.payload._id));
    }
    default:
        return state;
    }
};
