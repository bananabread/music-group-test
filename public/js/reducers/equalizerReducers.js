export default (state = [], action) => {
    switch (action.type) {
    case 'GET_EQUALIZERS_SUCCESS': {
        return action.equalizers;
    }
    case 'CREATE_EQUALIZER_SUCCESS': {
        return [
            ...state,
            Object.assign({}, action.payload),
        ];
    }
    case 'UPDATE_EQUALIZER_SUCCESS': {
        return state.map((equalizer) => {
            if (equalizer._id !== action.payload._id) return equalizer;
            return action.payload;
        });
    }
    case 'DELETE_EQUALIZER_SUCCESS': {
        return state.filter(equalizer => (equalizer._id !== action.payload._id));
    }
    default:
        return state;
    }
};
