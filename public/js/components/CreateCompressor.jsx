import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Tickmarks from './Tickmarks';
import * as compressorActions from '../actions/compressorActions';

class CreateCompressor extends React.Component {
    constructor(props) {
        super(props);
    }

    submitForm(e) {
        e.preventDefault();
        const data = {
            name: this.name.value,
            mode: this.mode.value,
            attack: this.attack.value,
            release: this.release.value,
            threshold: this.threshold.value,
            ratio: this.ratio.value,
            presence: this.presence.value,
            make_up: this.make_up.value,
        };
        this.props.createCompressor(data);
        browserHistory.push('/');
    }

    render() {
        return (
          <div>
            <h3>Create Compressor</h3>
            <form onSubmit={e => this.submitForm(e)}>
              <div className="form-group">
                <label htmlFor="name">Preset Name</label>
                <input className="form-control" type="text" id="name" name="name" ref={node => this.name = node} required />
              </div>

              <div className="form-group">
                <label htmlFor="mode">Mode</label>
                <select className="form-control" id="mode" name="mode" ref={node => this.mode = node}>
                  <option value="creative">Creative</option>
                  <option value="default">Default</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="attack">Attack (ms)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="attack" name="attack" min="0" max="100" step="1" ref={node => this.attack = node} />
              </div>

              <div className="form-group">
                <label htmlFor="release">Release (ms)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="release" name="release" min="0" max="100" step="1" ref={node => this.release = node} />
              </div>

              <div className="form-group">
                <label htmlFor="threshold">Threshold (dB)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="threshold" name="threshold" min="0" max="100" step="1" ref={node => this.threshold = node} />
              </div>

              <div className="form-group">
                <label htmlFor="ratio">Ratio</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="ratio" name="ratio" min="0" max="100" step="1" ref={node => this.ratio = node} />
              </div>

              <div className="form-group">
                <label htmlFor="presence">Presence (dB)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="presence" name="presence" min="0" max="100" step="1" ref={node => this.presence = node} />
              </div>

              <div className="form-group">
                <label htmlFor="make_up">Make Up (dB)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="make_up" name="make_up" min="0" max="100" step="1" ref={node => this.make_up = node} />
              </div>

              <input type="submit" value="Submit" className="btn btn-success" />

              <Tickmarks />
            </form>
          </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = dispatch => ({
    createCompressor: compressor => dispatch(compressorActions.createCompressor(compressor)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateCompressor);
