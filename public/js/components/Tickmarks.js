import React from 'react';

const Tickmarks = ({name}) => (
    <div>
        <datalist id="tickmarks-10-10">
            <option value="-10" label="-10"></option>
            <option value="9" label="-9"></option>
            <option value="8" label="-8"></option>
            <option value="7" label="-7"></option>
            <option value="6" label="-6"></option>
            <option value="5" label="-5"></option>
            <option value="4" label="-4"></option>
            <option value="3" label="-3"></option>
            <option value="2" label="-2"></option>
            <option value="1" label="-1"></option>
            <option value="0" label="0"></option>
            <option value="1" label="1"></option>
            <option value="2" label="2"></option>
            <option value="3" label="3"></option>
            <option value="4" label="4"></option>
            <option value="5" label="5"></option>
            <option value="6" label="6"></option>
            <option value="7" label="7"></option>
            <option value="8" label="8"></option>
            <option value="9" label="9"></option>
            <option value="10" label="10"></option>
        </datalist>

        <datalist id="tickmarks1-100">
            <option value="0" label="0"></option>
            <option value="10" label="10"></option>
            <option value="20" label="20"></option>
            <option value="30" label="30"></option>
            <option value="40" label="40"></option>
            <option value="50" label="50"></option>
            <option value="60" label="60"></option>
            <option value="70" label="70"></option>
            <option value="80" label="80"></option>
            <option value="90" label="90"></option>
            <option value="100" label="100"></option>
        </datalist>

        <datalist id="tickmarks5-200">
            <option value="0"></option>
            <option value="5"></option>
            <option value="10"></option>
            <option value="15"></option>
            <option value="20"></option>
            <option value="25"></option>
            <option value="30"></option>
            <option value="35"></option>
            <option value="40"></option>
            <option value="45"></option>
            <option value="50"></option>
            <option value="55"></option>
            <option value="60"></option>
            <option value="65"></option>
            <option value="70"></option>
            <option value="75"></option>
            <option value="80"></option>
            <option value="85"></option>
            <option value="90"></option>
            <option value="95"></option>
            <option value="100"></option>
            <option value="105"></option>
            <option value="110"></option>
            <option value="115"></option>
            <option value="120"></option>
            <option value="125"></option>
            <option value="130"></option>
            <option value="135"></option>
            <option value="140"></option>
            <option value="145"></option>
            <option value="150"></option>
            <option value="155"></option>
            <option value="160"></option>
            <option value="165"></option>
            <option value="170"></option>
            <option value="175"></option>
            <option value="180"></option>
            <option value="185"></option>
            <option value="190"></option>
            <option value="195"></option>
            <option value="200"></option>
        </datalist>
    </div>
);

export default Tickmarks;