import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Tickmarks from './Tickmarks';
import * as compressorActions from '../actions/compressorActions';

class Compressor extends React.Component {
    constructor(props) {
        super(props);
    }

    submitForm(e) {
        e.preventDefault();
        const data = {
            name: this.name.value,
            mode: this.mode.value,
            attack: this.attack.value,
            release: this.release.value,
            threshold: this.threshold.value,
            ratio: this.ratio.value,
            presence: this.presence.value,
            make_up: this.make_up.value,
        };
        this.props.updateCompressor(this.props.compressor._id, data);
        browserHistory.push('/');
    }

    render() {
        return (
          <div>
            <h3>Update Compressor</h3>
            <form onSubmit={e => this.submitForm(e)}>
              <div className="form-group">
                <label htmlFor="name">Preset Name</label>
                <input className="form-control" type="text" id="name" name="name" defaultValue={this.props.compressor.name} ref={node => this.name = node} required />
              </div>

              <div className="form-group">
                <label htmlFor="mode">Mode</label>
                <select className="form-control" id="mode" name="mode" defaultValue={this.props.compressor.mode} ref={node => this.mode = node}>
                  <option value="creative">Creative</option>
                  <option value="default">Default</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="attack">Attack (ms)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="attack" name="attack" defaultValue={this.props.compressor.attack} min="0" max="100" step="1" ref={node => this.attack = node} />
              </div>

              <div className="form-group">
                <label htmlFor="release">Release (ms)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="release" name="release" defaultValue={this.props.compressor.release} min="0" max="100" step="1" ref={node => this.release = node} />
              </div>

              <div className="form-group">
                <label htmlFor="threshold">Threshold (dB)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="threshold" name="threshold" defaultValue={this.props.compressor.threshold} min="0" max="100" step="1" ref={node => this.threshold = node} />
              </div>

              <div className="form-group">
                <label htmlFor="ratio">Ratio</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="ratio" name="ratio" defaultValue={this.props.compressor.ratio} min="0" max="100" step="1" ref={node => this.ratio = node} />
              </div>

              <div className="form-group">
                <label htmlFor="presence">Presence (dB)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="presence" name="presence" defaultValue={this.props.compressor.presence} min="0" max="100" step="1" ref={node => this.presence = node} />
              </div>

              <div className="form-group">
                <label htmlFor="make_up">Make Up (dB)</label>
                <input className="form-control" type="range" list="tickmarks1-100" id="make_up" name="make_up" defaultValue={this.props.compressor.make_up} min="0" max="100" step="1" ref={node => this.make_up = node} />
              </div>

              <input type="submit" value="Update" className="btn btn-success" />

              <Tickmarks />
            </form>
          </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    compressor: state.compressors[ownProps.params.id],
});

const mapDispatchToProps = dispatch => ({
    updateCompressor: (index, compressor) => dispatch(compressorActions.updateCompressor(index, compressor)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Compressor);
