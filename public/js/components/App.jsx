import React from 'react';
import { Link } from 'react-router';

const App = props => (
  <div>
    <nav className="navbar navbar-default">
      <Link to="/" className="navbar-brand">Music Presets App</Link>
      <ul className="nav navbar-nav pull-right">
        <li><Link to="/equalizer/create">Create Equalizer</Link></li>
        <li><Link to="/compressor/create">Create Compressor</Link></li>
      </ul>
    </nav>
    <div className="container">{props.children}</div>
  </div>
    );

export default App;
