import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import Tickmarks from './Tickmarks';
import * as equalizerActions from '../actions/equalizerActions';

class CreateEqualizer extends React.Component {
    constructor(props) {
        super(props);
    }

    submitForm(e) {
        e.preventDefault();
        const data = {
            name: this.name.value,
            low_band: this.low_band.value,
            low_peak: this.low_peak.value,
            low_freq: this.low_freq.value,
            low_gain: this.low_gain.value,
            mid_band: this.mid_band.value,
            mid_q: this.mid_q.value,
            mid_freq: this.mid_freq.value,
            mid_gain: this.mid_gain.value,
            high_mid_band: this.high_mid_band.value,
            high_mid_freq: this.high_mid_freq.value,
            high_mid_gain: this.high_mid_gain.value,
            high_band: this.high_band.value,
            high_peak: this.high_peak.value,
            high_freq: this.high_freq.value,
            high_gain: this.high_gain.value,
        };
        this.props.createEqualizer(data);
        browserHistory.push('/');
    }

    render() {
        return (
          <div>
            <h3>Create Equalizer</h3>
            <form onSubmit={e => this.submitForm(e)}>
              <div className="form-group">
                <label htmlFor="name">Preset Name</label>
                <input className="form-control" type="text" id="name" name="name" ref={node => this.name = node} required />
              </div>

              <div className="form-group">
                <label htmlFor="low_band">Low Band</label>
                <select className="form-control" id="low_band" name="low_band" ref={node => this.low_band = node}>
                  <option value="on">On</option>
                  <option value="off">Off</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="low_peak">Low Peak/Shelf</label>
                <select className="form-control" id="low_peak" name="low_peak" ref={node => this.low_peak = node}>
                  <option value="peak">Peak</option>
                  <option value="shelf">Shelf</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="low_freq">Low Freq (Hz)</label>
                <input className="form-control" type="range" id="low_freq" name="low_freq" min="0" max="200" step="5" list="tickmarks5-200" ref={node => this.low_freq = node} />
              </div>

              <div className="form-group">
                <label htmlFor="low_gain">Low Gain</label>
                <input className="form-control" type="range" id="low_gain" name="low_gain" min="-10" max="10" step="1" list="tickmarks-10-10" ref={node => this.low_gain = node} />
              </div>

              <div className="form-group">
                <label htmlFor="mid_band">Low/Mid Band</label>
                <select className="form-control" id="mid_band" name="mid_band" ref={node => this.mid_band = node}>
                  <option value="On">On</option>
                  <option value="Off">Off</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="mid_q">Low/Mid Hi/Low Q</label>
                <select className="form-control" id="mid_q" name="mid_q" ref={node => this.mid_q = node}>
                  <option value="hi">Hi</option>
                  <option value="low">Low</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="mid_freq">Low/Mid Freq (Hz)</label>
                <input className="form-control" type="range" id="mid_freq" name="mid_freq" min="0" max="200" step="5" list="tickmarks5-200" ref={node => this.mid_freq = node} />
              </div>

              <div className="form-group">
                <label htmlFor="mid_gain">Low/Mid Gain</label>
                <input className="form-control" type="range" id="mid_gain" name="mid_gain" min="-10" max="10" step="1" list="tickmarks-10-10" ref={node => this.mid_gain = node} />
              </div>

              <div className="form-group">
                <label htmlFor="high_mid_band">High/Mid Band</label>
                <select className="form-control" id="high_mid_band" name="high_mid_band" ref={node => this.high_mid_band = node}>
                  <option value="On">On</option>
                  <option value="Off">Off</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="high_mid_freq">High/Mid Freq (kHz)</label>
                <input className="form-control" type="range" id="high_mid_freq" name="high_mid_freq" min="0" max="200" step="5" list="tickmarks5-200" ref={node => this.high_mid_freq = node} />
              </div>

              <div className="form-group">
                <label htmlFor="high_mid_gain">High/Mid Gain</label>
                <input className="form-control" type="range" id="high_mid_gain" name="high_mid_gain" min="-10" max="10" step="1" list="tickmarks-10-10" ref={node => this.high_mid_gain = node} />
              </div>

              <div className="form-group">
                <label htmlFor="high_band">High Band</label>
                <select className="form-control" id="high_band" name="high_band" ref={node => this.high_band = node}>
                  <option value="On">On</option>
                  <option value="Off">Off</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="high_peak">High Peak/Shelf</label>
                <select className="form-control" id="high_peak" name="high_peak" ref={node => this.high_peak = node}>
                  <option value="peak">Peak</option>
                  <option value="shelf">Shelf</option>
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="high_freq">High Freq (kHz)</label>
                <input className="form-control" type="range" id="high_freq" name="high_freq" min="0" max="200" step="5" list="tickmarks5-200" ref={node => this.high_freq = node} />
              </div>

              <div className="form-group">
                <label htmlFor="high_gain">High Gain</label>
                <input className="form-control" type="range" id="high_gain" name="high_gain" min="-10" max="10" step="1" list="tickmarks-10-10" ref={node => this.high_gain = node} />
              </div>

              <input type="submit" value="Submit" className="btn btn-success" />

              <Tickmarks />
            </form>
          </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({});

const mapDispatchToProps = dispatch => ({
    createEqualizer: equalizer => dispatch(equalizerActions.createEqualizer(equalizer)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateEqualizer);
