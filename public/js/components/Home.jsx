import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import * as equalizerActions from '../actions/equalizerActions';
import * as compressorActions from '../actions/compressorActions';

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    deleteEqualizer(index) {
        this.props.deleteEqualizer(index);
    }

    deleteCompressor(index) {
        this.props.deleteCompressor(index);
    }

    render() {
        return (
          <div className="row">

            <div className="col-lg-6">
                <h1>Equalizers</h1>
                <ul className="list-group">
                    {this.props.equalizers.map((equalizer, index) => (
                        <li className="list-group-item" key={index}>
                            <button className="btn btn-danger btn-xs pull-right" type="button" onClick={e => this.deleteEqualizer(equalizer._id)}>Delete Preset</button>
                            <Link to={`/equalizer/${index}`}>{ equalizer.name }</Link>
                        </li>
                    ))}
                </ul>
            </div>

            <div className="col-lg-6">
                <h1>Compressors</h1>
                <ul className="list-group">
                    {this.props.compressors.map((compressor, index) => (
                        <li className="list-group-item" key={index}>
                            <button className="btn btn-danger btn-xs pull-right" type="button" onClick={e => this.deleteCompressor(compressor._id)}>Delete Preset</button>
                            <Link to={`/compressor/${index}`}>{ compressor.name }</Link>
                        </li>
                    ))}
                </ul>
            </div>

          </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        equalizers: state.equalizers,
        compressors: state.compressors,
    };
};

const mapDispatchToProps = dispatch => ({
    deleteEqualizer: id => dispatch(equalizerActions.deleteEqualizer(id)),
    deleteCompressor: id => dispatch(compressorActions.deleteCompressor(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
