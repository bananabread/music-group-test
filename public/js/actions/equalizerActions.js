import Axios from 'axios';

const apiUrl = `${API_URL}/equalizer`;

export const getEqualizerSuccess = equalizers => ({
    type: 'GET_EQUALIZERS_SUCCESS',
    equalizers,
});

export const getEqualizers = () => dispatch => Axios.get(apiUrl)
        .then((response) => {
            dispatch(getEqualizerSuccess(response.data));
        })
        .catch((error) => {
            throw (error);
        });

export const createEqualizerSuccess = equalizer => ({
    type: 'CREATE_EQUALIZER_SUCCESS',
    payload: equalizer,
});

export const createEqualizer = equalizer => dispatch => Axios.post(apiUrl, equalizer)
        .then((response) => {
            dispatch(createEqualizerSuccess(response.data));
        })
        .catch((error) => {
            throw (error);
        });

export const updateEqualizerSuccess = equalizer => ({
    type: 'UPDATE_EQUALIZER_SUCCESS',
    payload: equalizer,
});

export const updateEqualizer = (index, equalizer) => dispatch => Axios.put(`${apiUrl}/${index}`, equalizer)
        .then((response) => {
            dispatch(updateEqualizerSuccess(response.data));
        })
        .catch((error) => {
            throw (error);
        });

export const deleteEqualizerSuccess = equalizer => ({
    type: 'DELETE_EQUALIZER_SUCCESS',
    payload: equalizer,
});

export const deleteEqualizer = index => dispatch => Axios.delete(`${apiUrl}/${index}`)
        .then((response) => {
            dispatch(deleteEqualizerSuccess(response.data));
        })
        .catch((error) => {
            throw (error);
        });
