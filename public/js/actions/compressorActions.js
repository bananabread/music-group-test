import Axios from 'axios';

const apiUrl = `${API_URL}/compressor`;

export const getCompressorsSuccess = compressors => ({
    type: 'GET_COMPRESSORS_SUCCESS',
    compressors,
});

export const getCompressors = () => dispatch => Axios.get(apiUrl)
        .then((response) => {
            dispatch(getCompressorsSuccess(response.data));
        })
        .catch((error) => {
            throw (error);
        });

export const createCompressorSuccess = compressor => ({
    type: 'CREATE_COMPRESSOR_SUCCESS',
    payload: compressor,
});

export const createCompressor = compressor => dispatch => Axios.post(apiUrl, compressor)
        .then((response) => {
            dispatch(createCompressorSuccess(response.data));
        })
        .catch((error) => {
            throw (error);
        });

export const updateCompressorSuccess = compressor => ({
    type: 'UPDATE_COMPRESSOR_SUCCESS',
    payload: compressor,
});

export const updateCompressor = (index, compressor) => dispatch => Axios.put(`${apiUrl}/${index}`, compressor)
        .then((response) => {
            dispatch(updateCompressorSuccess(response.data));
        })
        .catch((error) => {
            throw (error);
        });

export const deleteCompressorSuccess = compressor => ({
    type: 'DELETE_COMPRESSOR_SUCCESS',
    payload: compressor,
});

export const deleteCompressor = index => dispatch => Axios.delete(`${apiUrl}/${index}`)
        .then((response) => {
            dispatch(deleteCompressorSuccess(response.data));
        })
        .catch((error) => {
            throw (error);
        });
