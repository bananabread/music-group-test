import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';

import configureStore from './store/configureStore';
import '../styles/styles.scss';
import { syncHistoryWithStore } from 'react-router-redux';

import routes from './routes.jsx';

import * as compressorActions from './actions/compressorActions';
import * as equalizerActions from './actions/equalizerActions';

const store = configureStore();

store.dispatch(compressorActions.getCompressors());
store.dispatch(equalizerActions.getEqualizers());

const history = syncHistoryWithStore(browserHistory, store);

render(
  <Provider store={store}>
    <Router history={history} routes={routes} />
  </Provider>,
    document.getElementById('app'),
);
