import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App.jsx';
import Home from './components/Home.jsx';
import Equalizer from './components/Equalizer.jsx';
import Compressor from './components/Compressor.jsx';
import CreateEqualizer from './components/CreateEqualizer.jsx';
import CreateCompressor from './components/CreateCompressor.jsx';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="/equalizer/create" component={CreateEqualizer} />
    <Route name="equalizer" path="/equalizer/:id" component={Equalizer} />
    <Route path="/compressor/create" component={CreateCompressor} />
    <Route name="compressor" path="/compressor/:id" component={Compressor} />
  </Route>
);
