const mocha = require('mocha');
const expect = require('chai').expect;
const sinon = require('sinon');
const Equalizer = require('../../server/models/Equalizer');

let testEqualizer;

describe('Equalizer Model', () => {

    it('Should require all parameters in order to save', (done) => {
        const equalizer = new Equalizer();
        equalizer.validate((error) => {
            expect(error.errors.low_freq).to.exist;
            expect(error.errors.low_gain).to.exist;
            expect(error.errors.mid_freq).to.exist;
            expect(error.errors.mid_gain).to.exist;
            expect(error.errors.high_mid_freq).to.exist;
            expect(error.errors.high_mid_gain).to.exist;
            expect(error.errors.high_freq).to.exist;
            expect(error.errors.high_gain).to.exist;
            done();
        });
    });

    it('Should return a validation error when incorrect types passed', (done) => {
        const equalizer = new Equalizer({
            low_freq: 'TEST',
            low_gain: 'TEST',
            mid_freq: 'TEST',
            mid_gain: 'TEST',
            high_mid_freq: 'TEST',
            high_mid_gain: 'TEST',
            high_freq: 'TEST',
            high_gain: 'TEST',
        });

        equalizer.validate((error) => {
            expect(error.errors.low_freq).to.exist;
            expect(error.errors.low_gain).to.exist;
            expect(error.errors.mid_freq).to.exist;
            expect(error.errors.mid_gain).to.exist;
            expect(error.errors.high_mid_freq).to.exist;
            expect(error.errors.high_mid_gain).to.exist;
            expect(error.errors.high_freq).to.exist;
            expect(error.errors.high_gain).to.exist;
            done();
        });
    });

    beforeEach(() => {
        testEqualizer = new Equalizer({
            name: 'Test Equalizer',
            low_band: true,
            low_peak: 'peak',
            low_freq: 155,
            low_gain: 2,
            mid_band: true,
            mid_q: 'Hi',
            mid_freq: 290,
            mid_gain: -6,
            high_mid_band: true,
            high_mid_freq: 2.4,
            high_mid_gain: -2,
            high_band: true,
            high_peak: 'shelf',
            high_freq: 7.2,
            high_gain: 4,
        });
    });

    it('Should save a valid set of parameters', (done) => {
        testEqualizer.validate((error) => {
            expect(error).to.be.null;
            done();        
        });
    });

    it('Should update the updatedAt column', (done) => {
        expect(testEqualizer.updatedAt).to.be.ok;
        done();
    });

});
