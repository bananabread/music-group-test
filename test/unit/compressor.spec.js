const mocha = require('mocha');
const expect = require('chai').expect;
const sinon = require('sinon');
const Compressor = require('../../server/models/Compressor');

let testCompressor;

describe('Compressor Model', () => {

    it('Should require all parameters in order to save', (done) => {
        const compressor = new Compressor();
        compressor.validate((error) => {
            expect(error.errors.mode).to.exist;
            expect(error.errors.attack).to.exist;
            expect(error.errors.release).to.exist;
            expect(error.errors.threshold).to.exist;
            expect(error.errors.ratio).to.exist;
            expect(error.errors.presence).to.exist;
            expect(error.errors.make_up).to.exist;
            done();
        });
    });

    it('Should return a validation error when incorrect types passed', (done) => {
        const compressor = new Compressor({
            attack: 'TEST',
            release: 'TEST',
            threshold: 'TEST',
            ratio: 'TEST',
            presence: 'TEST',
            make_up: 'TEST',
        });

        compressor.validate((error) => {
            expect(error.errors.mode).to.exist;
            expect(error.errors.attack).to.exist;
            expect(error.errors.release).to.exist;
            expect(error.errors.threshold).to.exist;
            expect(error.errors.ratio).to.exist;
            expect(error.errors.presence).to.exist;
            expect(error.errors.make_up).to.exist;
            done();
        });
    });

    beforeEach(() => {
        testCompressor = new Compressor({
            name: 'Test Compressor',
            mode: 'creative',
            attack: 20,
            release: 10,
            threshold: -10,
            ratio: 7,
            presence: -7,
            make_up: 8,
        });
    });

    it('Should save a valid set of parameters', (done) => {
        testCompressor.validate((error) => {
            expect(error).to.be.null;
            done();        
        });
    });

    it('Should update the updatedAt column', (done) => {
        expect(testCompressor.updatedAt).to.be.ok;
        done();
    });

});
