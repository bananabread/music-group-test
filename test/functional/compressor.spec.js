const mocha = require('mocha');
const request = require('supertest-as-promised');
const expect = require('chai').expect;
const sinon = require('sinon');

const app = require('../../server/index');

const testCompressor = {
    name: 'Test compressor',
    mode: 'creative',
    attack: 20,
    release: 10,
    threshold: -10,
    ratio: 7,
    presence: -7,
    make_up: 8,
};

let testId;

describe('Compressor', () => {

    it('Should create an compressor', (done) => {
        request(app)
            .post('/api/compressor')
            .send(testCompressor)
            .expect(200)
            .then((response) => {
                expect(response.body.mode).to.equal('creative');
                expect(response.body.attack).to.equal(20);
                testId = response.body._id;
                done();
            })
            .catch(done);
    });

    it('Should return an compressor with a given _id', (done) => {
        request(app)
            .get(`/api/compressor/${testId}`)
            .expect(200)
            .then((response) => {
                expect(response.body.mode).to.equal('creative');
                expect(response.body.attack).to.equal(20);
                done();
            })
            .catch(done);
    });

    it('Should update an compressor', (done) => {
        testCompressor.mode = 'basic';
        request(app)
            .put(`/api/compressor/${testId}`)
            .send(testCompressor)
            .expect(200)
            .then((response) => {
                expect(response.body.mode).to.equal('basic');
                done();
            })
            .catch(done);
    });

    it('Should delete an compressor', (done) => {
        request(app)
            .delete(`/api/compressor/${testId}`)
            .send(testCompressor)
            .expect(200)
            .then((response) => {
                done();
            })
            .catch(done);
    });

});
