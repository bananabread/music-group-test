const mocha = require('mocha');
const request = require('supertest-as-promised');
const expect = require('chai').expect;
const sinon = require('sinon');

const app = require('../../server/index');

const testEqualizer = {
    name: 'Test Equalizer',
    low_band: true,
    low_peak: 'peak',
    low_freq: 155,
    low_gain: 2,
    mid_band: true,
    mid_q: 'Hi',
    mid_freq: 290,
    mid_gain: -6,
    high_mid_band: true,
    high_mid_freq: 2.4,
    high_mid_gain: -2,
    high_band: true,
    high_peak: 'shelf',
    high_freq: 7.2,
    high_gain: 4,
};

let testId;

describe('Equalizer', () => {

    it('Should create an equalizer', (done) => {
        request(app)
            .post('/api/equalizer')
            .send(testEqualizer)
            .expect(200)
            .then((response) => {
                expect(response.body.low_band).to.be.true;
                expect(response.body.low_freq).to.equal(155);
                testId = response.body._id;
                done();
            })
            .catch(done);
    });

    it('Should return an equalizer with a given _id', (done) => {
        request(app)
            .get(`/api/equalizer/${testId}`)
            .expect(200)
            .then((response) => {
                expect(response.body.low_band).to.be.true;
                expect(response.body.low_freq).to.equal(155);
                done();
            })
            .catch(done);
    });

    it('Should update an equalizer', (done) => {
        testEqualizer.low_band = false;
        request(app)
            .put(`/api/equalizer/${testId}`)
            .send(testEqualizer)
            .expect(200)
            .then((response) => {
                expect(response.body.low_band).to.be.false;
                done();
            })
            .catch(done);
    });

    it('Should delete an equalizer', (done) => {
        request(app)
            .delete(`/api/equalizer/${testId}`)
            .send(testEqualizer)
            .expect(200)
            .then((response) => {
                done();
            })
            .catch(done);
    });

});
