# Music Group Presets Test

The API is built with Express, designed to use MongoDB. I used Mongoose as the ORM to avoid writing vanilla MongoDB queries and calls, and make CRUD operations in general more generic and manageable. 

The front-end is built with React, React Router, Redux and Redux Thunk to allow asyncronous action response logic; along with Bootstrap for basic styling.

## Running the app

First run
```
yarn install
```

To run the application locally you will also need to create a .env file in the root of the repository, denoting the port, api url and mongo url. If running via docker you need to omit the MONGO_URL from the .env file. See example below;

```
PORT=8000
API_URL=http://localhost:8000/api
MONGO_URL=mongodb://localhost:27017/music-presets
```

Alternatively, just rename the .env.example file in the root of the repository to .env

Running the following command to start the app.
```
yarn start
```

The application can also be run as a docker container. Running;

```
docker-compose up
```

Will spin up the application in a docker container, mapped to port 8000 locally. It will also run a MongoDB container alongside to act as the database.

The application is also running on a Heroku container service app;
```
https://music-presets.herokuapp.com/
```

## Tests

There are both functional and unit tests for the application. Functional tests will require mongo to be running locally in order to run. To run the tests use;
```
yarn test
```

## Linting

There is a .eslintrc file to configure ESLint settings for the application. The config is based on the AirBnB preset.
```
yarn lint
```